-- === Autorun programs ===

local autorunApps =
{
	-- Compositor
	--"xcompmgr",	-- Nicht mehr sichtbar: tasklist, statusbar
	--"unagi",	-- Nach Links/Rechts-Verschieben Mausbewegung benötigt
	--"compton",	-- Veraltet, nicht mehr gepflegt
	"picom -b",

	-- GUI
	"xdg-user-dirs-update",
	"numlockx on",
	"mate-settings-daemon",
	"xbindkeys",
	--"light-locker",

	-- Applets
	"redshift-gtk",
	"nm-applet",
	"mate-volume-control-status-icon",
	"system-config-printer-applet",
	"mate-power-manager",
}

for app = 1, #autorunApps do
	awful.util.spawn( autorunApps[app] )
--	awful.spawn.once( autorunApps[app] )
--	awful.spawn.single_instance( autorunApps[app] )
end


--[[
'systemctl --user start autostart.target'
### keine Ahnung ###
/usr/bin/gnome-keyring-daemon --start --components=ssh
/usr/bin/gnome-keyring-daemon --start --components=secrets
/usr/bin/gnome-keyring-daemon --start --components=pkcs11

/usr/libexec/at-spi-bus-launcher --launch-immediately
/usr/libexec/geoclue-2.0/demos/agent
/usr/bin/pkcs11-register
/usr/libexec/polkit-mate-authentication-agent-1

### nicht nötig ###
xiccd
mate-screensaver
--]]
