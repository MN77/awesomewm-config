function screen_next(c)
	if multihead and c.screen == screen[1] then
		c:move_to_screen(screen[2])
		swap_master(c)
		return nil
	end

	--[[
	-- Stop on last
	local fs = awful.screen.focused()
	if fs.selected_tag == fs.tags[9] then
		return nil
	end

	if c then
		local cur_tag = c.first_tag
		if cur_tag then
			local i = cur_tag.index
			local cur_screen = c.screen
			local dest_tag = cur_screen.tags[i+1]
			if dest_tag then
				c:move_to_tag(dest_tag)

				view_tag(i) -- go back to current
				-- Move both screens + 1
				if multihead then
					if cur_screen == screen[2] then
						awful.tag.viewnext(screen[1])
						awful.tag.viewnext(screen[2])
					else
						awful.tag.viewnext(screen[2])
						awful.tag.viewnext(screen[1])
					end
				else
					awful.tag.viewnext(screen[1])
				end

				c:raise()
			end
		end
	end
	--]]
end

function screen_prev(c)
	if multihead and c.screen == screen[2] then
		c:move_to_screen(screen[1])
		swap_master(c)
		return nil
	end

	--[[
	-- Stop on last
	local fs = awful.screen.focused()
	if fs.selected_tag == fs.tags[1] then
		return nil
	end

	if c then
		local cur_tag = c.first_tag
		if cur_tag then
			local i = cur_tag.index
			local cur_screen = c.screen
			local dest_tag = cur_screen.tags[i-1]
			if dest_tag then
				c:move_to_tag(dest_tag)

				view_tag(i) -- go back to current
				-- Move on both screens
				if multihead then
					if cur_screen == screen[2] then
						awful.tag.viewprev(screen[1])
						awful.tag.viewprev(screen[2])
					else
						awful.tag.viewprev(screen[2])
						awful.tag.viewprev(screen[1])
					end
				else
					awful.tag.viewprev(screen[1])
				end

				c:raise()
			end
		end
	end
	--]]
end


