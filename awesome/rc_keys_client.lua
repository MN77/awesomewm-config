-- === Client key bindings ===

clientkeys = gears.table.join(
	--[[ Führt nur zu Problemen -> Fullscreen Layout oder F11 verwenden!
	awful.key({ modkey }, "f",
	awful.key({ modkey }, "Insert",
		function (c)
			c.fullscreen = not c.fullscreen
			c:raise()
		end,
		{description = "toggle fullscreen", group = "client"}),
	--]]

	awful.key({ modkey }, "Escape", -- r ist belegt!
		function (c)
			c.minimized = true
			c.minimized = false
		end,
		{description = "caret lost fix", group = "client"}),

	awful.key({ modkey }, "Page_Up",
		function (c)
			client_tile(c)
		end,
		{description = "set tile", group = "client"}),
	awful.key({ modkey }, "t",
		function (c)
			client_tile(c)
		end,
		{description = "set tile", group = "client"}),

	awful.key({ modkey }, "Page_Down",
		function (c)
			client_float(c)
		end,
		{description = "set floating", group = "client"}),
	awful.key({ modkey }, "f",
		function (c)
			client_float(c)
		end,
		{description = "set floating", group = "client"}),

	--[[
	awful.key({ modkey }, "Insert",
		function (c)
			--awful.client.floating.toggle
			
			-- c.floating = true
			c.floating = not c.floating
			
			if c.floating then
				local top_titlebar = awful.titlebar(c, {
					height	= 20,
					bg_normal = '#555555'
				})
				
				local buttons = gears.table.join(
					awful.button({ }, 1, function()
						client.focus = c
						c:raise()
						awful.mouse.client.move(c)
					end),
					awful.button({ }, 3, function()
						client.focus = c
						c:raise()
						awful.mouse.client.resize(c)
					end)
				)
				
				setup_titlebar( top_titlebar, c, buttons )
			else
				awful.titlebar.hide(c)
			end

			c:raise()
		end,
		{description = "toggle floating", group = "client"}),
	--]]

	--[[ Nicht mehr erlaubt
	awful.key({ modkey }, "n",
	--awful.key({ modkey }, "Delete",
	awful.key({ modkey }, "-",
		function (c)
			-- The client currently has the input focus, so it cannot be
			-- minimized, since minimized clients can't have the focus.
			c.minimized = true
		end,
		{description = "minimize", group = "client"}),
	--]]

	awful.key({ modkey }, "Delete",
		function (c)
			c:kill()
		end,
		{description = "close", group = "client"}),
	awful.key({ }, "Pause",
		function (c)
			c:kill()
		end,
		{description = "close", group = "client"}),
	awful.key({ modkey }, "q",
		function (c) 
			c:kill()
		end,
		{description = "close", group = "client"}),
	awful.key({ modkey }, "w",
		function (c)
			c:kill()
		end,
		{description = "close", group = "client"}),

	awful.key({ modkey }, "Right",
		function (c)
			screen_next(c)
		end,
		{description = "move to next screen", group = "client: screen"}),
	awful.key({ modkey }, "l", -- Tut nicht, warum auch immer!
		function (c)
			screen_next(c)
		end,
		{description = "move to next screen", group = "client: screen"}),

	awful.key({ modkey }, "Left",
		function (c)
			screen_prev(c)
		end,
		{description = "move to previous screen", group = "client: screen"}),
	awful.key({ modkey }, "h",
		function (c)
			screen_prev(c)
		end,
		{description = "move to previous screen", group = "client: screen"}),

	awful.key({ modkey, "Shift" }, "Right",
		function (c)
			pull_next()
		end,
		{description = "pull to next tag", group = "client: tag"}),
	awful.key({ modkey, "Shift" }, "l",
		function (c)
			pull_next()
		end,
		{description = "pull to next tag", group = "client: tag"}),

	awful.key({ modkey, "Shift" }, "Left", 
		function (c)
			pull_prev()
		end,
		{description = "pull to previous tag", group = "client: tag"}),
	awful.key({ modkey, "Shift" }, "h", 
		function (c)
			pull_prev()
		end,
		{description = "pull to previous tag", group = "client: tag"}),

	awful.key({ modkey, "Shift" }, "Up",
		function (c)
			pull_first(c)
		end,
		{description = "pull to first tag", group = "client: tag"}),
	awful.key({ modkey, "Shift" }, "k",
		function (c)
			pull_first(c)
		end,
		{description = "pull to first tag", group = "client: tag"}),

	awful.key({ modkey, "Shift" }, "Down",
		function (c)
			pull_last(c)
		end,
		--{description = "move to slave", group = "client: screen"}),
		{description = "pull to last tag", group = "client: tag"}),
	awful.key({ modkey, "Shift" }, "j",
		function (c)
			pull_last(c)
		end,
		--{description = "move to slave", group = "client: screen"}),
		{description = "pull to last tag", group = "client: tag"}),


	awful.key({ modkey }, "Home",
		function (c)
			swap_master(c)
		end,
		{description = "move to master", group = "client"}),
	awful.key({ modkey }, "a",
		function (c)
			swap_master(c)
		end,
		{description = "move to master", group = "client"}),

	awful.key({ modkey }, "End",
		function (c)
			awful.client.setslave(c)
		end,
		{description = "move to slave", group = "client"}),
	awful.key({ modkey }, "e",
		function (c)
			awful.client.setslave(c)
		end,
		{description = "move to slave", group = "client"}),

	awful.key({ }, "Scroll_Lock",
		function (c)
			swap_toggle(c)
		end,
		{description = "toggle master/slave", group = "client"}),
	awful.key({ modkey }, "c",
		function (c)
			swap_toggle(c)
		end,
		{description = "toggle master/slave", group = "client"})


	--[[
	awful.key({ modkey }, "t",
		function (c)
			c.ontop = not c.ontop
		end,
		{description = "toggle keep on top", group = "client"})
	--]]

	--[[
	awful.key({ modkey }, "#",
		function (c)
			c.maximized = not c.maximized
			c:raise()
		end ,
		{description = "(un)maximize", group = "client"}),

	awful.key({ modkey, "Control" }, "#",
		function (c)
			c.maximized_vertical = not c.maximized_vertical
			c:raise()
		end ,
		{description = "(un)maximize vertically", group = "client"}),

	awful.key({ modkey, "Shift"	}, "#",
		function (c)
			c.maximized_horizontal = not c.maximized_horizontal
			c:raise()
		end ,
		{description = "(un)maximize horizontally", group = "client"})
	--]]
	
	--awful.key({ modkey, "Shift"   }, "y", awful.placement.centered),
)


-- === Client buttons ===
clientbuttons = gears.table.join(
	awful.button({ }, 1, function (c)
		c:emit_signal("request::activate", "mouse_click", {raise = true})
	end),
	awful.button({ modkey }, 1, function (c)
		c:emit_signal("request::activate", "mouse_click", {raise = true})
		awful.mouse.client.move(c)
	end),
	awful.button({ modkey }, 3, function (c)
		c:emit_signal("request::activate", "mouse_click", {raise = true})
		awful.mouse.client.resize(c)
	end)
)

