
-- === Widgets ===

-- Launcher menu
mylauncher = awful.widget.launcher( {
	image = beautiful.awesome_icon,
	menu = mymainmenu
})

-- Keyboard map indicator and switcher
--mykeyboardlayout = awful.widget.keyboardlayout()


-- Create a text-datetime-clock widget
mydatetime = wibox.widget.textclock( " %A, %d.%m.%Y   %H:%M ", 60 )
mydatetime:connect_signal("button::press",
	function(self, lx, ly, button, mods, metadata)
		--wibox.widget.calendar.year(os.date('*t'))
		awful.util.spawn( "gsimplecal" )
	end)

-- Create a textclock widget
--mytextclock = wibox.widget.textclock( " %d.%m.%Y    %H:%M ", 60 )
mytextclock = wibox.widget.textclock( " %H:%M ", 60 )
mytextclock:connect_signal("button::press",
	function(self, lx, ly, button, mods, metadata)
		--wibox.widget.calendar.year(os.date('*t'))
		awful.util.spawn( "gsimplecal" )
	end)

-- USV-load
local myusvload = awful.widget.watch(
    "usvload watt",
    30, -- seconds
    function(widget, stdout)
        widget:set_text( " "..stdout )
    end
)
myusvload.forced_width = 60

-- CPU-Temp
--[[
local cpu_temp_cmd = 'bash -c "sensors coretemp-isa-0000|grep Package|sed \'s#.*:  +\\(.*\\)°C  .*#\\1 °C#\'"'
local mycputemp = awful.widget.watch( cpu_temp_cmd, 15)
mycputemp.forced_width = 70
--]]

-- Awesome-wm-widgets
--http://pavelmakhov.com/awesome-wm-widgets/
--https://github.com/streetturtle/awesome-wm-widgets/tree/master
local logout_widget = require('logout-menu-widget.logout-menu')
--local volume_widget = require('awesome-wm-widgets.volume-widget.volume')
local cpu_widget = require('cpu-widget.cpu-widget')
--local ram_widget = require('ram-widget.ram-widget')

-- Lain-widgets
--https://github.com/lcpz/lain/tree/master/widget
--local lain_sysload_widget = require('lain.widget.sysload')

-- Vicious
--https://github.com/vicious-widgets/vicious/tree/master

-- Noobie
--https://github.com/streetturtle/noobie


-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
	awful.button({ }, 1, function(t)
		--t:view_only()
		view_tag(t.index)
	end),
	--[[
	awful.button({ modkey }, 1, function(t)
								if client.focus then
									client.focus:move_to_tag(t)
								end
							end),
	awful.button({ }, 3, awful.tag.viewtoggle),
	awful.button({ modkey }, 3, function(t)
								if client.focus then
									client.focus:toggle_tag(t)
								end
							end),
	--]]
	awful.button({ }, 5,
		function(t)
			-- Stop on last
			local fs = awful.screen.focused()
			if fs.selected_tag == fs.tags[9] then
				return nil
			end
			awful.tag.viewnext(t.screen)
		end),
	awful.button({ }, 4,
		function(t)
			-- Stop on first
			local fs = awful.screen.focused()
			if fs.selected_tag == fs.tags[1] then
				return nil
			end
			awful.tag.viewprev(t.screen)
		end)
)

-- TaskList
local tasklist_buttons = gears.table.join(
	awful.button({ }, 1, function (c)
		c:emit_signal(
			"request::activate",
			"tasklist",
			{raise = true}
		)
		swap_master(c)

		--[[
		if c == client.focus then
			c.minimized = true
		else
			c:emit_signal(
				"request::activate",
				"tasklist",
				{raise = true}
			)
		end
		--]]
	end),

	awful.button({ }, 2, function (c)
		c:kill()
	end),

	awful.button({ }, 3, function(c)
		if multihead then
			if c.screen == screen[1] then
				c:move_to_screen(screen[2])
				swap_master(c)
				return nil
			end
			if c.screen == screen[2] then
				c:move_to_screen(screen[1])
				swap_master(c)
				return nil
			end
		else
			awful.menu.client_list({ 
				theme = { width = 300 } 
			})
		end
	end)

	 --[[
	 awful.button({ }, 4, function ()
							 awful.client.focus.byidx(1)
							end),
	 awful.button({ }, 5, function ()
								awful.client.focus.byidx(-1)
							end)
	 --]]
)

-- Wallpaper
--[[
local function set_wallpaper(s)
	if beautiful.wallpaper then
		local wallpaper = beautiful.wallpaper
		-- If wallpaper is a function, call it with the screen
		if type(wallpaper) == "function" then
			wallpaper = wallpaper(s)
		end
		gears.wallpaper.maximized(wallpaper, s, true)
	end
end
--]]

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
--screen.connect_signal("property::geometry", set_wallpaper)

local systray = wibox.widget.systray()
-- Create a box around the systray
local tray = wibox.widget {
	systray,
	valign = 'center',
	halign = 'center',
	widget = wibox.container.place,
}

local spacer = wibox.widget {
	widget = wibox.widget.margin,
	forced_width = 10,
}


-- === WiBox for every screen ===

awful.screen.connect_for_each_screen( function(s)
	--set_wallpaper(s)

	-- Each screen has its own tag table.
	--awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])
	--awful.tag({ "1", "2", "3", "4", "A", "B", "C", "X", "M" }, s, awful.layout.layouts[1])
	--awful.tag({ "1", "2", "3", "4", "5", "6", "7", "X", "M" }, s, awful.layout.layouts[1])
	--awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])
	--awful.tag({ "1", "2", "3", "4", "5" }, s, awful.layout.layouts[1])
	awful.tag({ "1", "2", "3", "4", "5", "6", "7" }, s, awful.layout.layouts[1])

	-- Set default master width
	for idx,tag in ipairs(s.tags) do
		--tag.master_width_factor = 0.618
		--tag.master_width_factor = 0.55
		--tag.master_width_factor = 0.6
		-- tag.master_width_factor = 0.65
		-- +- 0.05
		tag.master_width_factor = 0.7
	end


	-- Promptbox for each screen
	s.mypromptbox = awful.widget.prompt()
	
	-- Imagebox widget with icon indicating which layout we're using. One layoutbox per screen.
	s.mylayoutbox = awful.widget.layoutbox(s)
	s.mylayoutbox:buttons(gears.table.join(
		awful.button({ }, 1, function () awful.layout.inc( 1) end),
		awful.button({ }, 3, function () awful.layout.inc(-1) end),
		awful.button({ }, 4, function () awful.layout.inc( 1) end),
		awful.button({ }, 5, function () awful.layout.inc(-1) end)))
	
	-- Create a taglist widget
	s.mytaglist = awful.widget.taglist {
		screen	= s,
		filter	= awful.widget.taglist.filter.all,
		--filter	= awful.widget.taglist.filter.noempty,
		buttons = taglist_buttons,
		--source = function() return root.tags() end
	}

	-- Create a tasklist widget
	s.mytasklist = awful.widget.tasklist {
		screen	= s,
		filter	= awful.widget.tasklist.filter.currenttags,
		buttons = tasklist_buttons,
		
		layout   = {
		    spacing = 3,
		    spacing_widget = {
		        {
		            forced_width = 0,
		            --shape        = gears.shape.circle,
	            	widget       = wibox.widget.separator,
		        },
		        valign = 'center',
		        halign = 'center',
		        widget = wibox.container.place,
		    },
		    layout  = wibox.layout.flex.horizontal
		},
		-- Notice that there is *NO* wibox.wibox prefix, it is a template,
		-- not a widget instance.
		widget_template = {
		    {
		        {
		            {
		                {
		                    id     = 'icon_role',
		                    widget = wibox.widget.imagebox,
		                },
		                margins = 5,
		                widget  = wibox.container.margin,
		            },
		            {
		                id     = 'text_role',
		                widget = wibox.widget.textbox,
		            },
		            layout = wibox.layout.fixed.horizontal,
		        },
		        left  = 10,
		        right = 10,
		        widget = wibox.container.margin
		    },
		    id     = 'background_role',
		    widget = wibox.container.background,

		    -- ToolTip
		    create_callback = function(self, c, index, objects)
			local tooltip = awful.tooltip({
				objects = { self },
				timer_function = function()
					return c.name
				end,
				delay_show = 1,
			})

			tooltip.mode = "outside"
			--tooltip.preferred_positions = {"bottom"}
		    end,
		},
	}

	-- Create the wibox
	s.mywibox = awful.wibar({
		position = "top",
		screen = s,
		--ontop = true,
		ontop = false,
		--opacity = 1
	})

	-- Set size of the systray
	systray.base_size = s.mywibox.height * 0.9


	-- Right widgets
	local rightwidgets = nil

	if not multihead then
		rightwidgets = {
			layout = wibox.layout.fixed.horizontal,
			s.mypromptbox,
			--myusvload,
			tray,

			-- default
			--volume_widget(),
			-- customized
			--volume_widget{
			--	widget_type = 'arc'
			--},

			logout_widget(),
			mydatetime,
			--mytextclock,
			s.mylayoutbox,
		}
	else
		if s == screen[2] then
			-- last screen
			rightwidgets = {
				layout = wibox.layout.fixed.horizontal,
				s.mypromptbox,
--				weather_widget(),
				spacer,
				myusvload,

				--ram_widget( {
				--	--color_used = '#84AAD8',
				--	--color_used = '#b2cced',
				--	color_used = '#77ff77',
				--	color_free = '#222222',
				--	color_buf = '#bbbb00',
				--	widget_width = 30,
				--	widget_height = 30,
				--	widget_show_buf = true,
				--	timeout = 0.1,
				--}),
				cpu_widget( {
					width = 30,
					step_width = 1,
					step_spacing = 1,
					color = beautiful.fg_normal
				}),

				spacer,
				mydatetime,
				spacer,
				s.mylayoutbox,
			}
		else
			-- other screens
			rightwidgets = {
				layout = wibox.layout.fixed.horizontal,
				s.mypromptbox,
				spacer,
				tray,
				spacer,
				logout_widget(),
				spacer,
				mytextclock,
				spacer,
				s.mylayoutbox,
			}
		end
	end

	------------------------------------------------------------
	-- Add widgets to the wibox
	s.mywibox:setup {
		layout = wibox.layout.align.horizontal,
		{ -- Left widgets
			layout = wibox.layout.fixed.horizontal,
			mylauncher,
			spacer,
			s.mytaglist,
			spacer,
		},
		s.mytasklist, -- Middle widget
		rightwidgets, -- Right widgets
	}
end)

