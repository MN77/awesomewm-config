
-- Load Debian menu entries
debian = require("debian.menu") -- no more local


-- === Menu - Create a launcher widget and a main menu ===
myawesomemenu = {
	{ "Hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
	{ "Manual", terminal .. " -e man awesome" },
--	{ "Edit config", editor_cmd .. " " .. awesome.conffile },
	{ "Restart", awesome.restart },
	{ "Quit", function() awesome.quit() end },
}

local menu_awesome = { "Awesome", myawesomemenu, beautiful.awesome_icon }
local menu_terminal = { "Terminal", terminal }

if has_fdo then
	mymainmenu = freedesktop.menu.build({
		before = { menu_awesome },
		after =	{ menu_terminal }
	})
else
	mymainmenu = awful.menu({
		items = {
					menu_awesome,
					{ "Debian", debian.menu.Debian_menu.Debian },
					menu_terminal,
				}
	})
end



-- TODO Hide Menu on leave
-- Tut, aber ist so quatsch :-D
--mylauncher:connect_signal("mouse::leave", function () mymainmenu:hide() end)

--mymainmenu:connect_signal("mouse::leave", function () mymainmenu:hide() end)
-- mouse::enter

--[[ Tut, aber auch quatsch
mymainmenu:get_root().wibox:connect_signal("mouse::leave", function(c)
    mymainmenu:hide()
end)
--]]


-- === Menubar configuration ===
-- Set the terminal for applications that require it
menubar.utils.terminal = terminal

