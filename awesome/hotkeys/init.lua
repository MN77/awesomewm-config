
local hotkeys_popup = {
  widget = require("hotkeys.widget"),
}

hotkeys_popup.show_help = hotkeys_popup.widget.show_help
return hotkeys_popup
