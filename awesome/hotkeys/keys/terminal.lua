
local hotkeys_popup = require("hotkeys.widget")
local terminal_rule = { class = { "Mate-terminal" } }

for group_name, group_data in pairs({
    ["terminal: lf"] = { color = "#b2cced", rule_any = terminal_rule },
--    ["terminal: micro"] = { color = "#b2cced", rule_any = terminal_rule },
--    ["terminal: cmus"] = { color = "#b2cced", rule_any = terminal_rule },
--    ["terminal: fzf"] = { color = "#b2cced", rule_any = terminal_rule },
    ["terminal"] = { color = "#b2cced", rule_any = terminal_rule },
    ["terminal: history"] = { color = "#b2cced", rule_any = terminal_rule },
}) do
    hotkeys_popup.add_group_rules(group_name, group_data)
end

local terminal_keys = {

    ["terminal: lf"] = {{
        modifiers = {},
        keys = {
		["Enter"] = "open",
		["Delete"] = "move to trash",
		e = "edit",
		l = "less",
		r = "rename",
		['y/x/c/v'] = "clear/cut/copy/paste",
		q = "quit",
		['s..'] = "sort",
		['g..'] = "go to",
		['t..'] = "trash",
		['p,$'] = "prompt",
		j = "jaymo",
		h = "toggle hidden",
		m = "make dir",
		--M = "take dir",
		a = "make file",
		['d..'] = "use current dir",
		['ß'] = "show result",
		R = "reload",
		['?'] = "doc",
--[[
		['j..'] = "jump vor/zrk",
		['xx'] = "delete",
		h = "home",
		l = "m lazpaint",
		v = "view",
		y = "path to clipboard",
		['.'] = "versteckte",
		['*'] = "all/invert",
		B = "bulkrename",
		C = "clear buffers",
		['c/X/p'] = "copy/cut/paste",
		d = "details",
		['E'] = "eject media",
		f = "fzf jump",
		['nd/f'] = "dir/file neu",
		['ma/d/s'] = "selmode all/dir/show",
		P = "print file",
		s = "speicherverbrauch",
		S = "sha256sum",
		w = "terminal x",
		U = "unselect"
--]]
        }
    }},
--[[
    ["terminal: fzf"] = {{
        modifiers = { "Ctrl" },
        keys = {
		t = "dateien",
		r = "history"
        }
    }, {
        modifiers = { "Mod1" },
        keys = {
		c = "verzeichnisse"
        }}, 
    },

	["terminal: micro"] = {{
		modifiers = { "Ctrl" },
		keys = {
			["7"] = "einauskommentieren",
			e = "eingabezeile",
			b = "kommandoprompt",
			l = "goto line",
			d = "duplicate line",
			r = "ruler"
		}	
	}, {
		modifiers = { "Mod1" },
		keys = {
			["9"] = "springe zur klammer",
			["0"] = "colorcolumn 0",
			["1"] = "colorcolumn 109",
			["2"] = "colorcolumn 22",
			["Enter"] = "insertline below",
			["s"] = "suche cursorstring",
			["r"] = "replace x y",
			["t"] = "tasten shortcuts ein/aus",
			S = "save as"
		}
	}},
--]]

	["terminal"] = {
		{
			modifiers = { "Ctrl" },
			keys = {
				["Right"] = "cursor one word right",
				["Left"] = "cursor one word left",
				["End"] = "cursor to end of line",
				["Home"] = "cursor to start of line",
				l = "clear",
				c = "cancel",
				u = "löschen bis anfang",
				k = "löschen bis ende",
				["Space"] = "execute autosuggestion",
			}
		},{
			modifiers = { "Ctrl", "Shift" },
			keys = {
				["Right"] = "cursor to end of line",
				["Left"] = "cursor to start of line",
			}
		},{
			modifiers = { "Mod1" },
			keys = {
				--d = "löschen bis wortende"
			}
		},{
			modifiers = { "Ctrl", "Mod1" },
			keys = {
				["Right"] = "delete to the right",
				["Left"] = "delete to the left",
			}
		},{
			modifiers = { "Shift" },
			keys = {
				["Tab"] = "undo last action",
			}
		}
	},

	["terminal: history"] = {
		{
			modifiers = { "Ctrl" },
			keys = {
				["Up"] = "history substring search backward",
				["Down"] = "history substring search forward",
			}
		},{
			modifiers = { "Mod1" },
			keys = {
				["Up"] = "history search backward",
				["Down"] = "history search forward",
			}
		}
	},





--[[
    ["terminal: cmus"] = {{
        modifiers = {},
        keys = {
		e = "edit/expand",
		C = "clear playlist",
		f = "filter",
		F = "folge",
		c = "clear filter",
		a = "add",
		N = "neue playlist",
		S = "save playlist as ...",
		d = "delete",
		i = "info",
		m = "modes all/artist/album",
		s = "shuffle",
		x = "mixtapes",
		z = "meizeig",
		['$'] = "shell",
		['1'] = "tree",
		['2'] = "sorted",
		['3'] = "playlist",
		['4'] = "filters",
		['5'] = "settings",
		[',.'] = "vol 5%",
		['?'] = "manual",
		['/'] = "suche"
        }
    }},
--]]
}

hotkeys_popup.add_hotkeys(terminal_keys)
