
local hotkeys_popup = require("hotkeys.widget")
local mpv_rule = { class = { "mpv" } }

for group_name, group_data in pairs({
    ["mpv"] = { color = "#FFFFFF", rule_any = mpv_rule }
}) do
    hotkeys_popup.add_group_rules(group_name, group_data)
end

local mpv_keys = {

    ["mpv"] = {{
        modifiers = { "Mod1" },
        keys = {
	    ['+ -'] = "zoom",
	    f = "filenavigator",
            ['Backspace'] = "zoom reset"
        }
    }, {
	modifiers = {},
	keys = {
            ['ri/le'] = 'vor/zurück 5 sek',
            ['up/dwn'] = 'vor/zurück 1 min',
            ['[ ]'] = 'geschwindigkeit 10%',
            ['{ }'] = 'geschwindigkeit halb/doppelt',
            ['Backspace'] = 'geschwindigkeit normal',
            ['Space'] = 'pause',
            Q = 'quit mit stand gespeichert',
            f = 'fullscreen',
            o = 'fortschrittsbalken',
            O = 'fortschritt permanent',
            h = 'home msk',
            ['1 2'] = 'kontrast',
            ['3 4'] = 'helligkeit',
            ['5 6'] = 'gamma',
            ['7 8'] = 'sättigung',
            ['9 0'] = 'volume',
            T = 'bleibe top',
	    q = 'quit'
    }
    }}
}

hotkeys_popup.add_hotkeys(mpv_keys)
