
local keys = {
--  firefox = require("awful.hotkeys_popup.keys.firefox"),
  --tmux = require("awful.hotkeys_popup.keys.tmux"),

  --qutebrowser = require("awful.hotkeys_popup.keys.qutebrowser"),
  --termite = require("awful.hotkeys_popup.keys.termite"),

  --firefox = require("hotkeys.keys.firefox"),
  -- libreoffice = require("hotkeys.keys.libreoffice"),
  --mpv = require("hotkeys.keys.mpv"),
  -- gpicview = require("hotkeys.keys.gpicview"),
  -- claws = require("hotkeys.keys.claws"),
  -- zathura = require("hotkeys.keys.zathura"),
  -- lazpaint = require("hotkeys.keys.lazpaint"),
  -- audacity = require ("hotkeys.keys.audacity"),

  tmux = require("hotkeys.keys.tmux"),
  terminal = require("hotkeys.keys.terminal"),
}
return keys