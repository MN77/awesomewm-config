
local hotkeys_popup = require("hotkeys.widget")
local fire_rule = { class = { "Firefox" } }

for group_name, group_data in pairs({
    ["firefox"] = { color = "#FFFFFF", rule_any = fire_rule }
}) do
    hotkeys_popup.add_group_rules(group_name, group_data)
end

local firefox_keys = {

    ["firefox"] = {{
        modifiers = { "Mod1" },
        keys = {
	['Left'] = "zurück",
	['Right'] = "vorwärts",
        ['Pos1'] = "startseite",
        ['1..9'] = "zu tab"
        }
    }, {
        modifiers = { "Ctrl" },
        keys = {
        -- t = 'neuer tab',
        w = 'schließe tab',
	l = 'urlbar',
	d = 'setze lesezeichen',
	s = 'seite speichern unter',
	b = 'bookmarks',
	-- n = 'neues fenster',
	f = 'in seite suchen',
        ['Up'] = 'seitenanfang',
        ['Down'] = 'seitenende',
	['+,-,0'] = 'zoom',
	['F5'] = 'reload inkl cache',
	O = 'lesezeichen verwalten',
        ['Tab'] = 'nächster tab'
        }

    }, {
        modifiers = { "Ctrl", "Mod1" },
        keys = {
        r = 'lesemodus an/aus'
    },
    }}
}

hotkeys_popup.add_hotkeys(firefox_keys)
