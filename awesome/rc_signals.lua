-- === Local functions ===

local function set_border(c)
--[[
	local s = awful.screen.focused()

	if c.fullscreen or c.maximized or c.below or (#s.tiled_clients == 1 and not c.floating) or (s.selected_tag and s.selected_tag.layout.name == 'max' and not c.floating) then
		c.shape = gears.shape.rectangle
		c.border_width = 0
		--s.selected_tag.gap = 0
	else
		c.shape = function(cr,w,h)
			--gears.shape.rounded_rect(cr,w,h,10) -- Width=3: 6 8 [9] -- Width=2: 6 [8]
			gears.shape.rectangle(cr,w,h)
		end
		c.border_width = beautiful.border_width
		--s.selected_tag.gap = beautiful.useless_gap
       	end
--]]

	if c.floating and not c.below and not c.maximized then
		c.border_width = beautiful.border_width
	else
		c.border_width = 0
	end
	
        -- Change border style --
        c.shape = function(cr,w,h)
                --gears.shape.rectangle(cr,w,h)
                --gears.shape.rounded_rect(cr,w,h,10) -- Width=3: 6 8 [9] -- Width=2: 6 [8]
                gears.shape.rounded_rect(cr,w,h,6) -- Width=3: 6 8 [9] -- Width=2: 6 [8]
                --gears.shape.partially_rounded_rect(cr,w,h,true,true,false,false,6)
        end
end


-- === Signals ===

client.connect_signal("property::size", set_border)
--client.connect_signal("request::border", set_border)

-- Signals to execute when a new client appears ===
client.connect_signal("manage", function (c)
	-- Set the windows at the slave,
	-- i.e. put it at the end of others instead of setting it master.
	--if not awesome.startup then
	--    awful.client.setslave(c)
	--end

	if awesome.startup
		and not c.size_hints.user_position
		and not c.size_hints.program_position then
			-- Prevent clients from being unreachable after screen count changes.
			awful.placement.no_offscreen(c)
	end

	-- Neuen Fenstern immer den Fokus geben!!!
	--c:raise()

	set_border(c)
end)

client.connect_signal("unmanage", function (c)
	-- Always focus master, when a window is closed. Ignore floating windows and dialogs
	local master = awful.client.getmaster()
	local cscreen = c.screen
	local slayout = awful.layout.get( cscreen )
	local lname = awful.layout.getname( slayout )

	--if master and not c.floating then
	if master and lname == "fairw" and not c.floating then
		client.focus = master
		master:raise()
	end

	-- Raise second, if master is conky!
       if master and master.class == "Conky" then
		awful.client.setslave(master)

		master = awful.client.getmaster()
		client.focus = master
		master:raise()
	end

	--set_border(c)
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules
client.connect_signal("request::titlebars", function(c)
	-- buttons for the titlebar
	local buttons = gears.table.join(
		awful.button({ }, 1, function()
			c:emit_signal("request::activate", "titlebar", {raise = true})
			awful.mouse.client.move(c)
		end),
		awful.button({ }, 3, function()
			c:emit_signal("request::activate", "titlebar", {raise = true})
			awful.mouse.client.resize(c)
		end)
	)

	setup_titlebar( awful.titlebar(c), c, buttons )
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
	c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)


-- Disable minimized windows on "max" layout
client.connect_signal("property::minimized", function(c)
        local cscreen = c.screen
        local slayout = awful.layout.get( cscreen )
        local lname = awful.layout.getname( slayout )

        if lname == "max" and not c.floating then
	    c.minimized = false
        end
end)


-- Disable maximized windows
client.connect_signal("property::maximized", function(c)
	if c.instance ~= "gsimplecal" then
		c.maximized = false
	end
end)


-- Floating is always top
client.connect_signal("property::floating", function(c)
	c.ontop = c.floating and c.class ~= "Conky" and not c.fullscreen
end)
