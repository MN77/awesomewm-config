
-- === Setup titlebar ===
function setup_titlebar( titlebar, c, buttons )

	local right = nil

	if c.type == "dialog" then
		right = { -- Right
			--awful.titlebar.widget.floatingbutton (c),
			--awful.titlebar.widget.minimizebutton (c),
			--awful.titlebar.widget.maximizedbutton(c),
			--awful.titlebar.widget.stickybutton	(c),
			--awful.titlebar.widget.ontopbutton	(c),
			awful.titlebar.widget.closebutton	(c),
			layout = wibox.layout.fixed.horizontal()
		}
	else
		right = { -- Right
			awful.titlebar.widget.floatingbutton (c),
			awful.titlebar.widget.minimizebutton (c),
			--awful.titlebar.widget.maximizedbutton(c),
			--awful.titlebar.widget.stickybutton	(c),
			--awful.titlebar.widget.ontopbutton	(c),
			awful.titlebar.widget.closebutton	(c),
			layout = wibox.layout.fixed.horizontal()
		}
	end

	titlebar : setup {
		{ -- Left
			awful.titlebar.widget.iconwidget(c),
			buttons = buttons,
			layout	= wibox.layout.fixed.horizontal
		},
		{ -- Middle
			{ -- Title
				align	= "center",
				widget = awful.titlebar.widget.titlewidget(c)
			},
			buttons = buttons,
			layout	= wibox.layout.flex.horizontal
		},
		right,
		layout = wibox.layout.align.horizontal
	}
end


--function file_exists(name)
--   local f = io.open(name, "r")
--   return f ~= nil and io.close(f)
--function path_exists(path)
	--local paths = require("paths")
--	return paths.filep(path)
--end


-- https://stackoverflow.com/questions/15429236/how-to-check-if-a-module-exists-in-lua
function lazyload(module)
    local function requiref(module)
        return require(module)
    end

    local ok, res = pcall(requiref,module)
    if res then
	return res
    else
	return nil
    end

--    if not(res) then
        -- Do Stuff when no module
end

