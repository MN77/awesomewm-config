-- https://awesomewm.org/doc/api/classes/client.html

-- === Rules - applied to new clients (through the "manage" signal) ===
-- xprop WM_CLASS
-- WM_CLASS(STRING) = <instance>, <class>
-- WM_CLASS(STRING) = "agave", "Agave"
----------------------------------------------------------------------

awful.rules.rules = {
	-- All clients will match this rule.
	{ rule = { },
		properties = {
			--border_width = beautiful.border_width, -- handled by signals
			border_color = beautiful.border_normal,
			focus = awful.client.focus.filter,
			raise = true,
			keys = clientkeys,
			buttons = clientbuttons,
			screen = awful.screen.preferred,

			--placement = awful.placement.no_overlap + awful.placement.no_offscreen
			--placement = awful.placement.under_mouse + awful.placement.no_offscreen
			placement = awful.placement.centered + awful.placement.no_offscreen,
			
			--floating = false, -- nicht gut
			maximized = false -- Per Standard nicht maximieren, sondern tile'n
		}
	},

	-- Floating clients.
	{ rule_any = {
		instance = {
			"DTA",	-- Firefox addon DownThemAll.
			"copyq",	-- Includes session name in class.
			"pinentry",
			
			"mate-calc",
			},
		class = {
			"Arandr",
			"Blueman-manager",
			"Gpick",
			"Kruler",
			"MessageWin",	-- kalarm.
			"Sxiv",
			"Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
			"Wpa_gui",
			"veromix",
			"xtightvncviewer",

			"Agave",
			"org-jaymo_lang-cli-Main_JayMo",
			"Revelation",
			"Flowbladesinglerender",
			},

		-- Note that the name property shown in xprop might be set slightly after creation of the client
		-- and the name shown there might not match defined rules here.
		name = { -- title
			"Event Tester",	-- xev.
			},
		role = {
			"AlarmWindow",	-- Thunderbird's calendar.
			"ConfigManager",	-- Thunderbird's about:config.
			"pop-up",		-- e.g. Google Chrome's (detached) Developer Tools.
			}
		},
		properties = {
			floating = true,
			titlebars_enabled = true
			--ontop = true
		}},

	-- Add titlebars
	--{ rule_any = { type = { "normal", "dialog" }
	{ rule_any = {
			type = { "dialog" }
		},
			properties = { 
				titlebars_enabled = true,
				floating = true
				--placement = awful.placement.under_mouse + awful.placement.no_offscreen
			}
		},

	-- Set Firefox to always map on the tag named "2" on screen 1.
	-- { rule = { class = "Firefox" },
	--	properties = { screen = 1, tag = "2" } },
	

	-- ### Maximized ###
	{ rule_any = {
		class = {
			"Firefox-esr",
			"Eclipse",
			"thunderbird-default"
			}
		},
		properties = {
		--	floating = false,
		--	maximized = true,
		},
		callback = function (c)
			lm = awful.layout.layouts[1]
			awful.layout.set( lm )
		end
		},


	-- ###########################################################
	-- ### Special rules ###

	{ rule = { instance = "gsimplecal" },
		properties = {
			-- x = 400
			maximized = true,
			--floating = true,
		},
		callback = function (c)
			local cur_screen = awful.screen.focused()
			local screen_area = cur_screen.workarea
			c:geometry( {x= (cur_screen.index * screen_area.width) - c.width - 4} )
		end
	},

	{ rule = { class = "Conky" },
		properties = {
			--border_width = 0,
			--floating = true,
			below = true,
			--focusable = false,
		}},

	{ rule = { name = "qiv" },
		properties = {
			floating = false,
			maximized = false,
			fullscreen = true,
			y=0,
		}},

	{ rule = { class = "Mate-terminal" },
		properties = {
			callback = function(c)
				-- awful.client.setslave(c)

				-- Tiled
				local s = awful.screen.focused()
				if #s.selected_tag.layout.name == 'max' then
					layout1 = awful.layout.layouts[1]
					awful.layout.set( layout1 )
				end
			end
		}},

}
