
-- === View specific tag ===
function view_tag( ti )
	-- local fc = awful.client.getmaster()
	--if fc then
	--	fs = fc.screen
	--end

	-- View tag on every screen
	-- for s in screen do
		-- local tag = s.tags[ti]
		-- if tag then
			-- tag:view_only()
		-- end
	-- end

	if multihead then
		local fs = awful.screen.focused()
		local s1 = screen[1]
		local s2 = screen[2]
	
		if fs == s1 then -- Keeps focus on current screen!!!
			s1 = screen[2]
			s2 = screen[1]
		end

		local tag = s1.tags[ti]
		if tag then
			tag:view_only()
		end
		local tag = s2.tags[ti]
		if tag then
			tag:view_only()
		end
	else
		local s1 = screen[1]
		local tag = s1.tags[ti]
		if tag then
			tag:view_only()
		end
	end
end

function view_ctag( i )
	local cs = awful.screen.focused()
	local tag = cs.tags[i]
	tag:view_only()
end

function view_stag( s, i )
	local cs = screen[s]
	local tag = cs.tags[i]
	tag:view_only()
end

-- === View next tag ===
function view_tag_next()
	-- Stop on last
	local fs = awful.screen.focused()
	--if fs.selected_tag == fs.tags[9] then
	--if fs.selected_tag == fs.tags[ fs.tags:count() ] then
	if fs.selected_tag ~= fs.tags[ tagmax ] then
		awful.tag.viewnext(fs)
	end

	if multihead then
		local ft = fs.selected_tag.index
			
		-- Move on other screens
		if fs == screen[1] then
			view_stag(2, ft)
		else
			view_stag(1, ft)
		end
	end

	--awful.screen.focus( fs )

	--out = io.open('/tmp/next.txt','a')
  --out:write(fs.index)
  --io.close(out)
end

-- === View previous tag ===
function view_tag_prev()
	-- Stop on first
	local fs = awful.screen.focused()
	if fs.selected_tag ~= fs.tags[1] then
		awful.tag.viewprev(fs)
	end

	if multihead then
		local ft = fs.selected_tag.index
		
		-- Move on other screens
		if fs == screen[1] then
			view_stag(2, ft)
		else
			view_stag(1, ft)
		end
	end

	--awful.screen.focus( fs )
end

function view_all_tags()
	awful.tag.viewnone( screen[1] )
	if multihead then
	    awful.tag.viewnone( screen[2] )
	end
    
	for i = 1, tagmax do
		local tag1 = screen[1].tags[i]
		if tag1 then
			awful.tag.viewtoggle( tag1 )
		end
		
		if multihead then
		    local tag2 = screen[2].tags[i]
		    if tag2 then
			    awful.tag.viewtoggle( tag2 )
		    end
	    end
	end
end

function pull_tag(i)
	local cf = client.focus
	if cf then
		local tag = cf.screen.tags[i]
		if tag then
			cf:move_to_tag(tag)
			view_tag(i)
			swap_master(cf)
			-- client.focus = cf
			cf:raise()
		end
	end
end

function pull_next()
	-- Stop on last
	local fs = awful.screen.focused()
	--if fs.selected_tag == fs.tags[9] then
	if fs.selected_tag == fs.tags[ tagmax ] then
		return nil
	end

	local c = client.focus
	if c then
		local cur_tag = c.first_tag
		if cur_tag then
			local cti = cur_tag.index
			local cur_screen = c.screen
			local nti = cti + 1
			local dest_tag = cur_screen.tags[nti]

			if dest_tag then
				c:move_to_tag(dest_tag)
				view_tag(nti)
				swap_master(c)
				c:raise()
			end
		end
	end
end

function pull_prev()
	-- Stop on first
	local fs = awful.screen.focused()
	if fs.selected_tag == fs.tags[1] then
		return nil
	end

	local c = client.focus
	if c then
		local cur_tag = c.first_tag
		if cur_tag then
			local cti = cur_tag.index
			local cur_screen = c.screen
			local nti = cti - 1
			local dest_tag = cur_screen.tags[nti]

			if dest_tag then
				c:move_to_tag(dest_tag)
				view_tag(nti)
				c:raise()
			end
		end
	end
end

function pull_first(c)
	--c:swap(awful.client.getmaster())

	local fs = awful.screen.focused()
	c:move_to_tag( fs.tags[1] )
	view_tag(1)
	swap_master(c)
end

function pull_last(c)
	--awful.client.setslave(c)

	local fs = awful.screen.focused()
	c:move_to_tag( fs.tags[ tagmax ] )
	view_tag( tagmax )

	--awful.client.setmaster(c) -- Tut nicht immer
	swap_master(c)
end

function push_tag(i)
	local cf = client.focus -- Necessary this way!
	if cf then
		local tag = cf.screen.tags[i]
		if tag then
			cf:move_to_tag(tag)
		end
	end
end

