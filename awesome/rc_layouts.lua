-- === Table of layouts to cover with awful.layout.inc, order matters. ===

awful.layout.layouts = {
	--awful.layout.suit.floating,
	awful.layout.suit.tile,
	require('layout.fairw'), -- Eigenentwicklung, früher default
	awful.layout.suit.max,	-- Oft gebraucht, deaktivieren zwingt zum aufräumen ;-)
	--awful.layout.suit.max.fullscreen, -- Taskleiste fehlt, Vollbild macht eigentlich nur einzeln Sinn, gibt ja auch noch F11

--	awful.layout.suit.spiral.dwindle, -- auf kleinem Bildschirm sinnfrei, auch sonst unpraktisch
	-- awful.layout.suit.tile.left,
	-- awful.layout.suit.tile.bottom,
	-- awful.layout.suit.tile.top,
--	awful.layout.suit.fair,
--	require('fairw'),
--	awful.layout.suit.fair.horizontal,	-- selten gebraucht
	--awful.layout.suit.spiral,
	--awful.layout.suit.max,
	--awful.layout.suit.magnifier,
	--awful.layout.suit.corner.nw,
	-- awful.layout.suit.corner.ne,
	-- awful.layout.suit.corner.sw,
	-- awful.layout.suit.corner.se,
}

