-- === Launcher key bindings ===

globalkeys = gears.table.join(
	globalkeys,

	-- SYSTEM ---------------------------------------
    awful.key({ modkey, "Control", "Shift" }, "Home", --r
		function ()
			awful.spawn.with_shell("systemctl reboot")
		end,
		{description = "reboot", group = "system"}),
    awful.key({ modkey, "Control", "Shift" }, "space", --s
		function ()
			awful.spawn.with_shell("dm-tool lock; systemctl suspend")
		end,
		{description = "suspend", group = "system"}),
    awful.key({ modkey, "Control", "Shift" }, "l",
		function ()
			awful.spawn.with_shell("dm-tool lock")
		end,
		{description = "lock", group = "system"}),

--    awful.key({ modkey, "Control", "Shift" }, "Escape",
    awful.key({ modkey, "Control", "Shift" }, "End",
		function ()
			awful.spawn.with_shell("systemctl poweroff")
		end,
		{description = "power off", group = "system"}),
    awful.key({ modkey, "Control", "Shift" }, "e",
		function ()
			awful.spawn.with_shell("systemctl poweroff")
		end,
		{description = "power off", group = "system"}),

	awful.key({ modkey, "Control", "Shift" }, "Delete",
		function ()
			awful.spawn.with_shell("systemctl poweroff")
		end,
		{description = "power off", group = "system"}),
    awful.key({ modkey, "Control", "Shift" }, "d",
		function ()
			awful.spawn.with_shell("systemctl poweroff")
		end,
		{description = "power off", group = "system"}),


	-- SCREENSHOT ---------------------------------------
  --  awful.key({ "Shift", "Control" }, "Print", -- TUT NICHT
  --  awful.key({ "Shift" }, "Print", -- TUT NICHT
    	--	awful.spawn.with_shell("mate-screenshot -a") -- Bereich

    awful.key({}, "Print",
    	function ()
    		awful.spawn.with_shell("mate-screenshot")
    	end,
        {description = "full", group = "screenshot"}),

    awful.key({ "Mod1" }, "Print",
    	function ()
    		-- awful.spawn.with_shell("kazam -w")
    		awful.spawn.with_shell("mate-screenshot -w")
		-- mate-screenshot -w	# Fenster wird evtl. nicht erkannt
		-- kazam -w				# ok, aber ohne Bild
		-- screengrab -a
		-- scrot -u -b
    	end,
        {description = "window", group = "screenshot"}),

    awful.key({ "Control" }, "Print",
    	function ()
    		awful.spawn.with_shell("mate-screenshot -i")
		end,
        {description = "interactive", group = "screenshot"}),
    awful.key({ "Mod1" }, "p",
    	function ()
    		awful.spawn.with_shell("mate-screenshot -i")
		end,
        {description = "interactive", group = "screenshot"}),


	-- STARTER ---------------------------------------

	-- Terminal
	awful.key({ modkey }, "Return",
		function ()
			awful.spawn(terminal)
		end,
		{description = "Terminal", group = "starter"})


)

