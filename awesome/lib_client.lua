
function swap_master( c )
	--awful.client.setmaster(c)     -- Tut manchmal nicht
	local master = awful.client.getmaster()
	if master then
		c:swap(master)
	end
end

function swap_slave( c )
	if c then
		awful.client.setslave(c)
	end
end

function swap_toggle(c)
	local master = awful.client.getmaster()
	if master and c == master then
		swap_slave(c)
	else
		swap_master(c)
	end
end


function swap_prev()
	local c = client.focus -- Necessary this way!
	if c.screen:get_clients(false)[1] == c then
		return nil
	else
		awful.client.swap.byidx( -1 )
	end
end

function swap_next()
	local c = client.focus -- Necessary this way!

	-- get with correct order
	local clients = c.screen:get_clients(false)
	--local clients = c.first_tag:clients() -- wrong order!
	local cur_tag = c.first_tag

	local last = c
	for k,v in pairs(clients) do
		if v.first_tag == cur_tag then
			last = v
		end
	end

	if c == last then
		return nil
	else
		awful.client.swap.byidx( 1 )
	end
end

function client_float(c)
	c.maximized = false

	--awful.client.floating.toggle

	c.floating = true
	-- c.floating = not c.floating

	--if c.floating then
	local top_titlebar = awful.titlebar(c, {
		height	= 20,
		bg_normal = '#555555'
	})

	local buttons = gears.table.join(
		awful.button({ }, 1, function()
			client.focus = c
			c:raise()
			awful.mouse.client.move(c)
		end),
		awful.button({ }, 3, function()
			client.focus = c
			c:raise()
			awful.mouse.client.resize(c)
		end)
	)

	setup_titlebar( top_titlebar, c, buttons )
	--else
	--	awful.titlebar.hide(c)
	--end

	c:raise()
end

function client_tile(c)
	c.floating = false
	awful.titlebar.hide(c)

	--c.maximized = not c.maximized
	--c.maximized = true

	c:raise()
end

function client_next()
	awful.client.focus.byidx( 1 )
	--awful.client.focus.history.previous()
	if client.focus then
		client.focus:raise()
	end
end

function client_prev()
	awful.client.focus.byidx( -1 )
	if client.focus then
		client.focus:raise()
	end
end

