---------------------------------------------------------------------------
--- Fair west layout module for awful.
--- (A rework of the fair layouts module.)
--
-- @author Michael Nitsche
-- @copyright 2024 Michael Nitsche
---------------------------------------------------------------------------
--- Fair layouts module for awful.
--
-- @author Josh Komoroske
-- @copyright 2012 Josh Komoroske
-- @module awful.layout
---------------------------------------------------------------------------

-- Grab environment we need
local ipairs = ipairs
local math = math

--- The fairh layout layoutbox icon.
-- @beautiful beautiful.layout_fairh
-- @param surface
-- @see gears.surface

--- The fairv layout layoutbox icon.
-- @beautiful beautiful.layout_fairv
-- @param surface
-- @see gears.surface

local fair = {}

local function do_fair(p)
    local wa = p.workarea
    local cls = p.clients

    if #cls > 0 then
        local rows, cols
        if #cls == 2 then
            rows, cols = 1, 2
        else
            rows = math.ceil(math.sqrt(#cls))
            cols = math.ceil(#cls / rows)
        end

        for k, c in ipairs(cls) do
            k = k - 1
            
            local places = rows * cols
            local jump = places - #cls
            if k >= rows-jump then
	            k = k + jump
			end

            local g = {}

            local row, col
            row = k % rows
            col = math.floor(k / rows)

            local lrows, lcols
            if jump > 0 and col == 0 then
                lrows = rows - jump
                lcols = cols
            else
                lrows = rows
                lcols = cols
            end

            if jump > 0 and col == 0 then
                g.height = math.ceil(wa.height / lrows)
                g.y = g.height * row
            else
                g.height = math.ceil(wa.height / lrows)
                g.y = g.height * row
            end


            if col == lcols - 1 then
                g.width = wa.width - math.ceil(wa.width / lcols) * col
                g.x = wa.width - g.width
            else
                g.width = math.ceil(wa.width / lcols)
                g.x = g.width * col
            end

            g.y = g.y + wa.y
            g.x = g.x + wa.x

            p.geometries[c] = g
        end
    end
end

--- Vertical fair west layout.
-- @param screen The screen to arrange.
fair.name = "fairw"
function fair.arrange(p)
    return do_fair(p)
end

return fair

