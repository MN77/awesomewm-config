-- === Global key bindings ===

globalkeys = gears.table.join(
 
	-- awesome
	awful.key({ modkey, "Control", "Shift" }, "r",
		awesome.restart,
		{description = "reload awesome", group = "awesome"}),
	awful.key({ modkey, "Control", "Shift" }, "q", awesome.quit,
		{description = "quit awesome", group = "awesome"}),

	-- tools
	--awful.key({ modkey }, "h",
	awful.key({ modkey }, "F1",
		hotkeys_popup.show_help,
		{description="show help", group="awesome"}),

	--[[
	awful.key({ modkey }, "m",
		function ()
			mymainmenu:show()
		end,
		{description = "show main menu", group = "awesome"}),
	--]]
	awful.key({ modkey, "Control", "Shift" }, "p",
		function ()
			awful.screen.focused().mypromptbox:run()
		end,
		{description = "run prompt", group = "awesome"}),
	--[[
	--awful.key({ modkey }, "l", -- "x"
	awful.key({ modkey, "Control" }, "l", -- "x"
				function ()
					awful.prompt.run {
					prompt		= "Lua: ",
					textbox		= awful.screen.focused().mypromptbox.widget,
					exe_callback = awful.util.eval,
					history_path = awful.util.get_cache_dir() .. "/history_eval"
					}
				end,
				{description = "run Lua prompt", group = "awesome"}),
	--]]

	--[[
	awful.key({ modkey }, "p", function() menubar.show() end,
				{description = "show the menubar", group = "launcher"})
	--]]
	
	awful.key({ modkey, "Control", "Shift" }, "j",
		function ()
			awful.spawn.easy_async_with_shell()
			awful.prompt.run {
				prompt		= "JayMo: ",
				textbox		= awful.screen.focused().mypromptbox.widget,
				exe_callback = function(cmd) awful.spawn( { terminal, "-x", "jaymo-sid", "-cnE", cmd } ) end,
				history_path = awful.util.get_cache_dir() .. "/history"
			}
		end,
		{description = "run JayMo prompt", group = "awesome"}),

	-- tags
	awful.key({ modkey, "Control" }, "Left",
		function ()
			view_tag_prev()
		end,
		{description = "view tags prev", group = "tags"}),
	awful.key({ modkey, "Control" }, "h",
		function ()
			view_tag_prev()
		end,
		{description = "view tags prev", group = "tags"}),

	awful.key({ modkey, "Control" }, "Right",
		function ()
			view_tag_next()
		end,
		{description = "view tags next", group = "tags"}),
	awful.key({ modkey, "Control" }, "l",
		function ()
			view_tag_next()
		end,
		{description = "view tags next", group = "tags"}),

	awful.key({ modkey, "Control" }, "Up",
		function ()
			view_tag(1)
		end,
		{description = "view tags first", group = "tags"}),
	awful.key({ modkey, "Control" }, "k",
		function ()
			view_tag(1)
		end,
		{description = "view tags first", group = "tags"}),

	awful.key({ modkey, "Control" }, "Down",
		function ()
			view_tag(tagmax)
		end,
		{description = "view tags last", group = "tags"}),
	awful.key({ modkey, "Control" }, "j",
		function ()
			view_tag(tagmax)
		end,
		{description = "view tags last", group = "tags"}),

	--awful.key({ modkey,			}, "Escape", awful.tag.history.restore,
	--			{description = "go back", group = "tag"}),


	-- layout type
	awful.key({ modkey }, "space",
		function ()
			awful.layout.inc( 1 )
		end,
		{description = "select next", group = "layout"}),
	awful.key({ modkey, "Shift"	}, "space",
		function ()
			awful.layout.inc(-1)
		end,
		{description = "select prev", group = "layout"}),

	-- layout
	awful.key({ modkey }, "+",
		function ()
			awful.tag.incmwfact( 0.05)
		end,
		{description = "increase master", group = "layout"}),
	awful.key({ modkey }, "-",
		function ()
			awful.tag.incmwfact(-0.05)
		end,
		{description = "decrease master", group = "layout"}),
	--[[  Mehr als 2 Spalten machen hier keinen Sinn! Mehr als 1 Master eigentlich auch nicht!
	awful.key({ modkey, "Shift" }, "+",	 function () awful.tag.incnmaster( 1, nil, true) end,
				{description = "increase the number of master clients", group = "layout"}),
	awful.key({ modkey, "Shift" }, "-",	 function () awful.tag.incnmaster(-1, nil, true) end,
				{description = "decrease the number of master clients", group = "layout"}),
	awful.key({ modkey, "Control" }, "+",	 function () awful.tag.incncol( 1, nil, true)	end,
				{description = "increase the number of columns", group = "layout"}),
	awful.key({ modkey, "Control" }, "-",	 function () awful.tag.incncol(-1, nil, true)	end,
				{description = "decrease the number of columns", group = "layout"}),
	--]]

	-- navigation
	awful.key({ modkey }, "u",
		awful.client.urgent.jumpto,
		{description = "jump to urgent client", group = "client"}),

	-- all tags
	awful.key({ modkey, "Control" }, "a",
		function()
			view_all_tags()
		end,
		{description = "view all tags", group = "tags"}),
--[[
	awful.key({ modkey, "Shift" }, "a",
		function ()
			awful.tag.viewnone( screen[1] )
			if multihead then
			    awful.tag.viewnone( screen[2] )
		    end
			view_tag(1)
		end,
		{description = "view only first tag", group = "tag"}),
--]]

	-- client order
	awful.key({ modkey }, "Down",
		function (c)
			swap_next()
		end,
		{description = "swap with next client", group = "client"}),
	awful.key({ modkey }, "j",
		function (c)
			swap_next()
		end,
		{description = "swap with next client", group = "client"}),
		
	awful.key({ modkey }, "Up",
		function (c)
			swap_prev()
		end,
		{description = "swap with prev client", group = "client"}),
	awful.key({ modkey }, "k",
		function (c)
			swap_prev()
		end,
		{description = "swap with prev client", group = "client"}),

	awful.key({ modkey }, "Tab",
		function ()
			client_next()
		end,
		{description = "focus client next", group = "focus"}),
	awful.key({ modkey }, "n",
		function ()
			client_next()
		end,
		{description = "focus client next", group = "focus"}),
		
	awful.key({ modkey, "Shift" }, "Tab",
		function ()
			client_prev()
		end,
		{description = "focus client prev", group = "focus"}),
	awful.key({ modkey, "Shift" }, "n",
		function ()
			client_prev()
		end,
		{description = "focus client prev", group = "focus"}),

	awful.key({ modkey, "Control" }, "Tab",
		function ()
			awful.screen.focus_relative( 1 )
		end,
		{description = "focus screen next", group = "focus"}),
	awful.key({ modkey, "Control" }, "n",
		function ()
			awful.screen.focus_relative( 1 )
		end,
		{description = "focus screen next", group = "focus"}),
		
	awful.key({ modkey, "Control", "Shift" }, "Tab",
		function ()
			awful.screen.focus_relative(-1)
		end,
		{description = "focus screen prev", group = "focus"}),
	awful.key({ modkey, "Control", "Shift" }, "n",
		function ()
			awful.screen.focus_relative(-1)
		end,
		{description = "focus screen prev", group = "focus"}),

	awful.key({ modkey }, "ä",
		function () awful.screen.focus_relative( 1 ) end,
		{description = "focus screen next", group = "focus"}),
		
	awful.key({ modkey }, "ö",
		function ()
			awful.screen.focus_relative(-1)
		end,
		{description = "focus screen prev", group = "focus"}),

	awful.key({ modkey }, ",",
		function ()
			awful.screen.focus( screen[1] )
		end,
		{description = "focus screen 1", group = "focus"}),
	awful.key({ modkey }, ".",
		function ()
			awful.screen.focus( screen[2] )
		end,
		{description = "focus screen 2", group = "focus"})


	-- Minimize
	--[[
	awful.key({ modkey, "Control" }, "n",
				function ()
					local c = awful.client.restore()
					-- Focus restored client
					if c then
					c:emit_signal(
						"request::activate", "key.unminimize", {raise = true}
					)
					end
				end,
				{description = "restore minimized", group = "client"})
	--]]
)


-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.

for i = 1, tagmax do
	globalkeys = gears.table.join(globalkeys,
		-- Move client to tag, but stay on current.
		awful.key({ modkey }, "#" .. i + 9,
			function (c)
				push_tag(i)
			end,
			{description = "move to tag #"..i, group = "client: tag"}),

		-- View tag only.
		awful.key({ modkey, "Control" }, "#" .. i + 9,
			function ()
				view_tag(i)
			end,
			{description = "view tags #"..i, group = "tags"}),

		-- Move client to tag.
		awful.key({ modkey, "Shift" }, "#" .. i + 9,
			function (c)
				pull_tag(i)
			end,
			{description = "pull to tag #"..i, group = "client: tag"})
	)
end

