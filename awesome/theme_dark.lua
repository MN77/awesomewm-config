-------------------------------
-- https://awesomewm.org/apidoc/documentation/06-appearance.md.html
-------------------------------


local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
--local themes_path = require("gears.filesystem").get_themes_dir()
local icons_path = "/home/mike/.config/awesome/icons/"


--== COLORS ==
local color_white = "#ffffff"
local color_black = "#000000"
local color_dark  = "#222222"
local color_main  = "#b2cced" -- "#b6def2" "#8ab4f8" "#8cb9d6" "#A9E4F7" "#49F1F2"
local color_light = "#dddddd" -- "#cccccc"
local color_grey  = "#888888"
--local color_  = "#3C98BD" -- petrol
local color_steel = "#4682B4"  -- steel blue
local color_border = color_steel


--== GRADIENT ==
local gradient_light = function( height )
	return gears.color({
		type = "linear",
		from = { 0, 0 },
		to = { 0, height },
		stops = {
--			{ 0, color_white },
--			{ 0.25, "#d5e0ed" },
--			{ 0.75, color_main },
--			{ 1, color_steel }

			{ 0, "#d5e0ed" },	-- main, saturation 10
			{ 0.25, color_white },
			{ 0.65, "#d5e0ed" }, -- 0.75 ?
			{ 1, color_main }
		},
	})
end

local gradient_dark = function( height )
	return gears.color({
		type = "linear",
		from = { 0, 0 },
		to = { 0, height },
		stops = {
			{ 0, "#234059" },	-- steel, value 35
			{ 0.25, "#2D5273" },	-- steel, value 45
			{ 1, "#0a121a" },	-- steel, value 10
		},
	})
end


--== BASICS ==
local theme = {}
--theme.font          = "sans 9"
--theme.font          = "Hack Nerd Font Regular 11"  -- 9
--theme.font          = "DejaVu Sans Regular 11"  -- 9
--theme.font          = "Nunito Regular 11"
--theme.font          = "Nunito SemiBold 11"
--theme.font          = "Droid Sans Regular 11"

--theme.font          = "Noto Sans Regular 11"  -- 9
--theme.font          = "Cantarell Regular 11"  -- 9
theme.font          = "Roboto Regular 11.5"

-- icon-theme muss ein Ordner unter /usr/share/icons sein
--theme.icon_theme = "Adwaita"	-- Tut noch nicht
--theme.icon_theme = "Tango"
theme.icon_theme = "gnome"


--== DEFAULT COLORS ==
theme.bg_normal     = color_black
theme.bg_focus      = color_main
theme.bg_urgent     = "#ff0000"
theme.bg_minimize   = color_grey --"#555555"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = color_light
theme.fg_focus      = color_black
theme.fg_urgent     = color_dark
theme.fg_minimize   = color_light

theme.gap_single_client = false  -- No gap when maximized!!!
theme.useless_gap   = dpi(2)
theme.border_width  = dpi(2)
theme.border_normal = color_border
theme.border_focus  = color_main
theme.border_marked = "#91231c"


--== LAYOUT IMAGES ==
theme.layout_fairh = icons_path.."layouts/fairhw.png"
theme.layout_fairv = icons_path.."layouts/fairvw.png"
theme.layout_fairw = icons_path.."layouts/fairvw.png"
theme.layout_floating  = icons_path.."layouts/floatingw.png"
theme.layout_magnifier = icons_path.."layouts/magnifierw.png"
theme.layout_max = icons_path.."layouts/maxw.png"
theme.layout_fullscreen = icons_path.."layouts/fullscreenw.png"
theme.layout_tilebottom = icons_path.."layouts/tilebottomw.png"
theme.layout_tileleft   = icons_path.."layouts/tileleftw.png"
theme.layout_tile = icons_path.."layouts/tilew.png"
theme.layout_tiletop = icons_path.."layouts/tiletopw.png"
theme.layout_spiral  = icons_path.."layouts/spiralw.png"
theme.layout_dwindle = icons_path.."layouts/dwindlew.png"
theme.layout_cornernw = icons_path.."layouts/cornernww.png"
theme.layout_cornerne = icons_path.."layouts/cornernew.png"
theme.layout_cornersw = icons_path.."layouts/cornersww.png"
theme.layout_cornerse = icons_path.."layouts/cornersew.png"

theme.awesome_icon           = "/usr/share/icons/gnome/32x32/places/debian-swirl.png"

-- from default for now...
theme.menu_submenu_icon     = icons_path .. "submenu.png"


--== WIBAR ==
theme.wibar_border_width = 1
theme.wibar_border_color = color_black -- color_dark -- '#777777'


--== TAG-LIST ==
--theme.taglist_spacing = 7
--theme.taglist_spacing     = 2
--theme.taglist_font        = "awesomewm 11"
--theme.taglist_font        = "Progress 11"

theme.taglist_squares       = "false"

--theme.taglist_bg_focus      = color_main
theme.taglist_bg_focus = gradient_light( 31 )
--theme.taglist_bg_occupied
theme.taglist_fg_empty    = color_light
--theme.taglist_fg_focus    = "#ffffff"
--theme.taglist_fg_occupied = "#164b5d"
theme.taglist_fg_occupied = "#ff0040" -- "#D70751" "#f23df2" "#f279f2" "#B10542"
--theme.taglist_fg_occupied =  --"#D70751" -- "#B10542"
--theme.taglist_fg_urgent   = "#ff0000"

theme.taglist_shape_border_width = 1
theme.taglist_shape_border_color = color_border
theme.taglist_shape_border_width_empty = 0

theme.taglist_shape_border_width_focus = 1
--theme.taglist_shape_border_color_focus = color_light
theme.taglist_shape_border_color_focus = color_border
theme.taglist_shape_focus = function(cr,w,h)
	gears.shape.rounded_rect(cr,w,h,6)
end
theme.taglist_shape_urgent = function(cr,w,h)
	gears.shape.rounded_rect(cr,w,h,6)
end



--== TASK-LIST ==
--theme.tasklist_bg_focus  = "#ffffff"
--theme.tasklist_bg_normal = "#b2cced" -- "#cccccc"
--theme.tasklist_bg_focus  = color_main
--theme.tasklist_bg_normal = color_dark -- "#cccccc"
theme.tasklist_bg_focus = gradient_light( 31 )
theme.tasklist_bg_normal = gradient_dark( 31 )

--theme.tasklist_align = "center"

theme.tasklist_shape_border_width = 1
theme.tasklist_shape_border_color = color_border
--theme.tasklist_shape_border_color_focus = color_light

--theme.tasklist_shape  = gears.shape.rounded_bar
theme.tasklist_shape = function(cr,w,h)
	gears.shape.rounded_rect(cr,w,h, 11) -- 10
end



--== MENU ==
theme.menu_height = dpi(30)
theme.menu_width  = dpi(200)


--== WALLPAPER ==
--theme.wallpaper             = icons_path .. "sky/sky-background.png"


--== TITLEBAR ==
theme.titlebar_close_button = "true"
--theme.titlebar_bg_focus = '#84AAD8'
--theme.titlebar_bg_normal = '#555555'

theme.titlebar_bgimage = function(context, cr, width, height)
	local pattern = nil

	if client.focus == context.client then
		pattern = gradient_light( height )
	else
		pattern = gradient_dark( height )
	end

	cr:set_source( pattern )
	cr:paint()
end

-- Define the images to load
theme.titlebar_close_button_normal = icons_path.."titlebar_bm/close_normal.svg"
theme.titlebar_close_button_focus = icons_path.."titlebar_bm/close_focus.svg"

theme.titlebar_minimize_button_normal = icons_path.."titlebar_bm/minimize_normal.svg"
theme.titlebar_minimize_button_focus  = icons_path.."titlebar_bm/minimize_focus.svg"

theme.titlebar_ontop_button_normal_inactive = icons_path.."titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive = icons_path.."titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = icons_path.."titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active = icons_path.."titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = icons_path.."titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive = icons_path.."titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = icons_path.."titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active = icons_path.."titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = icons_path.."titlebar_bm/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive = icons_path.."titlebar_bm/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = icons_path.."titlebar_bm/floating_normal_active.png"
theme.titlebar_floating_button_focus_active = icons_path.."titlebar_bm/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = icons_path.."titlebar_bm/maximized_normal_inactive.svg"
theme.titlebar_maximized_button_focus_inactive = icons_path.."titlebar_bm/maximized_focus_inactive.svg"
theme.titlebar_maximized_button_normal_active = icons_path.."titlebar_bm/maximized_normal_active.svg"
theme.titlebar_maximized_button_focus_active = icons_path.."titlebar_bm/maximized_focus_active.svg"


--== SNAP ==
theme.snap_bg = '#999999'
--theme.snap_border_width
--theme.snap_shape
--theme.snapper_gap


--== TOOLTIP ==
theme.tooltip_opacity = 1
theme.tooltip_bg = color_main
theme.tooltip_fg = color_black


--== NOTIFICATION ==
--naughty.notification.border_width = 
--naughty.notification.width = dpi(500)
--naughty.notification.timeout = 2
--naughty.notification.timeout = 2
--theme.notification.border_width = 10
--theme.notification_bg = "#0000FF"
--theme.notification_border_width = 2

theme.notification_shape = function(cr,w,h)
	gears.shape.rounded_rect(cr,w,h,6)
end

---------------------------
return theme
