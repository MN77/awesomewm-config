---------------------
local autorun = true
---------------------
--[[
autorun = false
--]]
---------------------

--https://awesomewm.org/doc/api/classes/client.html

-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")


-- Standard awesome library
gears = require("gears") -- no more local
awful = require("awful") -- no more local
require("awful.autofocus")
-- Widget and layout library
wibox = require("wibox") -- no more local
-- Theme handling library
beautiful = require("beautiful") -- no more local
-- Notification library
naughty = require("naughty") -- no more local
menubar = require("menubar") -- no more local

--hotkeys_popup = require("awful.hotkeys_popup") -- no more local
-- Enable hotkeys help widget for VIM and other apps when client with a matching name is opened:
--require("awful.hotkeys_popup.keys")
hotkeys_popup = require("hotkeys") -- no more local
require("hotkeys.keys")

has_fdo, freedesktop = pcall(require, "freedesktop") -- no more local


-- === Error handling ===
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
	naughty.notify({ preset = naughty.config.presets.critical,
		title = "Oops, there were errors during startup!",
		text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
	local in_error = false
	awesome.connect_signal("debug::error", function (err)
		-- Make sure we don't go into an endless error loop
		if in_error then return end
		in_error = true

		naughty.notify({ preset = naughty.config.presets.critical,
			 title = "Oops, an error happened!",
			 text = tostring(err) })
		in_error = false
	end)
end


-- === Variable definitions ===
-- This is used later as the default terminal and editor to run.
terminal = "mate-terminal"
editor = os.getenv("EDITOR") or "editor"
editor_cmd = terminal .. " -e " .. editor

multihead = screen:count() > 1
tagmax = 7 -- TODO automatisieren?

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- === Local RC ===
require('lib_rc')
require('lib_tag')
require('lib_screen')
require('lib_client')

local lrc = lazyload('local') or {}
local theme = lrc.theme or "theme_light"

-- === Themes define colours, icons, font and wallpapers ===
--beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
--beautiful.init( "/home/mike/.config/awesome/theme.lua")
beautiful.init( gears.filesystem.get_configuration_dir() .. theme .. ".lua")
--beautiful.gap_single_client = false  -- No gap when maximized!!!


-- === Self added globale settings ===
os.setlocale(os.getenv("LANG"))

require('rc_layouts')
require('rc_menu')
require('rc_screen')

-- === Set keys ===
require('rc_keys_global')
require('rc_keys_launcher')
require('rc_keys_client')

lazyload('shared.keys_launcher')
lazyload('local.keys_launcher')

require('rc_root')
require('rc_rules')
require('rc_signals')


if autorun then
	require('rc_autorun')

	lazyload('local.autorun')

	--if path_exists( "local.autorun" ) then
	--	require('local.autorun')
	--end
end


-- === TESTS / TODO ===
--[[
https://wiki.gentoo.org/wiki/Awesome#Date
https://stackoverflow.com/questions/41237181/lua-split-code-into-separate-files
require 'file'
require "codelibrary.variables";	--> codelibrary/variables.lua

https://github.com/pw4ever/awesome-wm-config
--]]

-- { "Shutdown", function () awesome.spawn("gdm-control --shutdown"); awesome.quit() end}
